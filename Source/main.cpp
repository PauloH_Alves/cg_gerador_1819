#include "gerador.h"
#include <iostream>
#include <string>




int main(int argc, char** argv) {

	//TODO fazer controle de erros 
	//TODO mudar para switch
	//TODO criar outras figuras geometricas
	if (argc < 2) return -1;
	
	if (std::string(argv[1]) == "plano") {
		gerar_plano(argv[2]);
	}
	if (std::string(argv[1]) == "bezier") {
		pontos_bezier_quadratica();
	}
	if (std::string(argv[1]) == "catmull") {
		npontos_catmull("catmull.txt");
	}
	if (std::string(argv[1]) == "caixa") {
		int tmx, tmy, tmz, stacks;
		tmx = std::stoi(argv[2]);
		tmy = std::stoi(argv[3]);
		tmz = std::stoi(argv[4]);
		stacks = std::stoi(argv[5]);
		gerar_caixa(tmx, tmy, tmz, stacks, argv[6]);
		//df
	}
	if (std::string(argv[1]) == "esfera") {
		float raio;
		int verticais, horizontais;
		raio = std::stof(argv[2]);
		verticais = std::stoi(argv[3]);
		horizontais = std::stoi(argv[4]);
		gerar_esfera(raio,verticais,horizontais,argv[5]);
	}
	if (std::string(argv[1]) == "cone") {
		float raio,altura;
		int verticais, horizontais;
		raio = std::stof(argv[2]);
		altura = std::stof(argv[3]);
		verticais = std::stoi(argv[4]);
		horizontais = std::stoi(argv[5]);
		gerar_cone(raio, altura, verticais, horizontais, argv[6]);
	}


	return 0;
}