#include "gerador.h"
#include <fstream>
#include <string>
#include <math.h>
#include <vector>

using namespace std;

float processingMap(float valor, float minInicial, float maxInicial, float minFinal, float maxFinal) {
	float outgoing = minFinal + (maxFinal - minFinal) * ((valor - minInicial) / (maxInicial - minInicial));
	return outgoing;

}

void gerar_plano(string ficheiro) {

	ofstream stream_escrita;
	stream_escrita.open(".\\modelos\\"+ficheiro);
	string virg = ",";

	stream_escrita << 4 << endl;
	stream_escrita << -1 << virg << 0 << virg << -1 << endl;
	stream_escrita << -1 << virg << 0 << virg << 1 << endl;
	stream_escrita << 1 << virg << 0 << virg << 1 << endl;
	stream_escrita << 1 << virg << 0 << virg << -1 << endl;
	
	//fechar stream(n necessario)
	stream_escrita.close();
	
}


void gerar_caixa(int tmx, int tmy, int tmz, int stacks,string ficheiro) {
	ofstream stream_escrita;
	stream_escrita.open(".\\modelos\\" + ficheiro);
	string virg = ",";
	stream_escrita << stacks << endl;
	if (stacks == 1) {
		stream_escrita << 8 << endl;
		//vertices com y=0
		stream_escrita << 0 << virg << 0 << virg << 0 << endl;
		stream_escrita << 0 << virg << 0 << virg << tmz << endl;
		stream_escrita << tmx << virg << 0 << virg << tmz << endl;
		stream_escrita << tmx << virg << 0 << virg << 0 << endl;
		//vertices com y=tmy
		stream_escrita << 0 << virg << tmy << virg << 0 << endl;
		stream_escrita << 0 << virg << tmy << virg << tmz << endl;
		stream_escrita << tmx << virg << tmy << virg << tmz << endl;
		stream_escrita << tmx << virg << tmy << virg << 0 << endl;
	}

}

void gerar_esfera(float raio, int verticais, int horizontais,string ficheiro) {
	ofstream stream_escrita;
	stream_escrita.open(".\\modelos\\" + ficheiro);
	string virg = ",";
	//gerar mapa 
	float latitude;
	float longitude;
	float x, y, z;
	//+1 primeiro vertice gerado manualmente
	int n_vertices = (verticais + 1) * ( horizontais) +1;
	//int conta_vertice = 0;
	stream_escrita << verticais << endl;
	stream_escrita << horizontais << endl;
	stream_escrita << n_vertices << endl;
	//adicionar primeiro vertice
	stream_escrita << 0 << virg << raio << virg << 0 << endl;
	// se come�ar em 0 gera lixo de j* com  0,raio,0
	for (int i = 1; i < horizontais+1; i++) {
		latitude = processingMap(i, 0, verticais, 0, M_PI);
				
		for (int j = 0; j < verticais +1; j++) {
			longitude = processingMap(j, 0, horizontais, 0, (2*M_PI));
			x = raio * sin(latitude)*cos(longitude);
			y = raio * sin(latitude)*sin(longitude);
			z = raio * cos(latitude);
			stream_escrita << x << virg << y << virg << z << endl;

		}
	}
	stream_escrita.close();




}
// alterei algo 


void gerar_cone(float raio, float altura, int verticais, int horizontais,string ficheiro){

	// circulo
	// x=raio*cos0
	// y=raio*sin0
	// r^2 = x^2 + y^2

	ofstream stream_escrita;
	stream_escrita.open(".\\modelos\\" + ficheiro);
	string virg = ",";
	float x, y,z;
	y = 0;
	stream_escrita << verticais << endl;
	stream_escrita << horizontais << endl;
	stream_escrita << verticais*horizontais + 1 << endl;
	float factor = M_PI / 180;
	float dois_pi = 2 * M_PI;
	float decremento = 360/verticais;
	float incremento_altura = altura / horizontais;
	float decremento_raio = raio / horizontais;


	for (int y = 0; y < altura; y+=incremento_altura) {
		for (int angulo = 0; angulo < 360; angulo += decremento) {
			x = raio * cos(angulo*factor);
			z = raio * sin(angulo*factor);
			stream_escrita << x << virg << y << virg << z << endl;
		}
		raio = raio - decremento_raio;

	}
	stream_escrita << 0 << virg << altura << virg << 0 << endl;


}

ponto ponto_bezier_quadratica(ponto pinicial, ponto pintermedio, ponto pfinal,float nvezes) {
	ponto temp;
	temp.x = (pow(1-nvezes,2) * pinicial.x) + (1 - nvezes) * 2 * nvezes * pintermedio.x + nvezes * nvezes * pfinal.x;
	temp.y = powf(1-nvezes,2) * pinicial.y + (1 - nvezes) * 2 * nvezes * pintermedio.y + nvezes * nvezes * pfinal.y;
	return temp;



}

void pontos_bezier_quadratica() {
	ponto pontos_curva[20];
	ponto np;
	ponto pinicial,  pintermedio, pfinal;
	pinicial.x = 0.0;
	pinicial.y = 0.0;
	pintermedio.x = 20.0;
	pintermedio.y = 20.0;
	pfinal.x = 40.0;
	pfinal.y = 5.0;
	int j = 0;
	float incremento = 0;
	for (float i = 0; i < 11; i ++) {
		np = ponto_bezier_quadratica(pinicial,pintermedio,pfinal,incremento);
		incremento = incremento + 0.1;
		pontos_curva[j] = np;
		j++;
	}

	
}

ponto3D ponto_catmull(ponto3D p0, ponto3D p1, ponto3D p2, ponto3D p3, float t) {

	
	float t2 = t * t;
	float t3 = t * t * t;
	ponto3D temp;
	temp.x = 0;
	temp.y = 0;
	temp.z = 0;
	temp.x = 0.5 *((2 * p1.x) +
			(-p0.x + p2.x) * t +
			(2 * p0.x - 5 * p1.x + 4 * p2.x - p3.x) * t2 +
			(-p0.x + 3 * p1.x - 3 * p2.x + p3.x) * t3);

	temp.y = 0.5 *((2 * p1.y) +
			(-p0.y + p2.y) * t +
			(2 * p0.y - 5 * p1.y + 4 * p2.y - p3.y) * t2 +
			(-p0.y + 3 * p1.y - 3 * p2.y + p3.y) * t3);

	temp.z = 0.5 *((2 * p1.z) +
			(-p0.z + p2.z) * t +
			(2 * p0.z - 5 * p1.z + 4 * p2.z - p3.z) * t2 +
			(-p0.z + 3 * p1.z - 3 * p2.z + p3.z) * t3);
	return temp;
		
}

void pontos_catmull(ponto3D p0, ponto3D p1, ponto3D p2, ponto3D p3, string ficheiro) {
	ofstream escrita;
	escrita.open(".\\modelos\\" + ficheiro);
	ponto3D pcalculado;

	for (float t = 0.0f; t < 1.0f; t += 0.05f) {
	
		pcalculado = ponto_catmull(p0, p1, p2, p3, t);
		escrita << pcalculado.x << "," << pcalculado.y << "," << pcalculado.z << endl;
	}
}

void npontos_catmull(string ficheiro) {
	ofstream escrita;
	escrita.open(".\\modelos\\" + ficheiro);
	std::vector<ponto3D> lp;
	ponto3D temp;
	temp.x = 0;
	temp.y = 0;
	temp.z = 0;
	lp.push_back(temp);
	temp.x = 5;
	temp.y = 0;
	temp.z = 0;
	lp.push_back(temp);
	temp.x = 0;
	temp.y = 0;
	temp.z = 5;
	lp.push_back(temp);
	temp.x = -5;
	temp.y = 0;
	temp.z = 0;
	lp.push_back(temp);
	temp.x = 0;
	temp.y = 0;
	temp.z = -5;
	lp.push_back(temp);

	//mm
	//svgxc
	//





	ponto3D pcalculado;
	float incremento = 0.05f;
	escrita << (1 / incremento)*4 << endl;
	for (float t = 0.0f; t < 1; t += incremento) {

		pcalculado = ponto_catmull(lp.at(0), lp.at(1), lp.at(2), lp.at(3), t);
		escrita << pcalculado.x << "," << pcalculado.y << "," << pcalculado.z << endl;
	}
	for (float t = 0.0f; t < 1; t += incremento) {

		pcalculado = ponto_catmull(lp.at(0), lp.at(2), lp.at(3), lp.at(4), t);
		escrita << pcalculado.x << "," << pcalculado.y << "," << pcalculado.z << endl;
	}
	for (float t = 0.0f; t < 1; t += incremento) {

		pcalculado = ponto_catmull(lp.at(0), lp.at(3), lp.at(4), lp.at(1), t);
		escrita << pcalculado.x << "," << pcalculado.y << "," << pcalculado.z << endl;
	}
	
	for (float t = 0.0f; t <= 1.05f; t += incremento) {

		pcalculado = ponto_catmull(lp.at(0), lp.at(4), lp.at(1), lp.at(2), t);
		escrita << pcalculado.x << "," << pcalculado.y << "," << pcalculado.z << endl;
	}

}