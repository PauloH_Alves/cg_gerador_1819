#include <string>
#ifndef GERADOR_H_
#define GERADOR_H_
#define _USE_MATH_DEFINES
using namespace std;


typedef struct ponto {
	float x;
	float y;
};

typedef struct ponto3D {
	float x;
	float y;
	float z;
};
//func map que converte valores de um espa�o para outro( baseada na func map do "processing 3" )
float processingMap(float valor, float minInicial, float maxInicial, float minFinal, float maxFinal);
//x,y e z correspondem ao vertice superior esquerdo do plano, o plano est� centrado em C={(x+tamanho)/2,y,(x+tamanho)/2}
void gerar_plano(std::string ficheiro); 
void gerar_caixa(int tmx, int tmy, int tmz, int stacks, std::string ficheiro);//TODO criar parti��es
void gerar_esfera(float raio, int verticais, int horizontais, std::string ficheiro);
void gerar_cone(float raio, float altura, int verticais, int horizontais, std::string ficheiro);//TODO corrigir erro de altura < divis�es
ponto ponto_bezier_quadratica(ponto pinicial, ponto pintermedio, ponto pfinal, int nvezes);
void pontos_bezier_quadratica();
ponto3D ponto_catmull(ponto3D p0, ponto3D p1, ponto3D p2, ponto3D p3, float t);
void pontos_catmull(ponto3D p0, ponto3D p1, ponto3D p2, ponto3D p3, string ficheiro);
void npontos_catmull(string ficheiro);

#endif